<?php
/**
 * Plugin Name: Content-Subscriber
 * Plugin URI:  https://opinfacts.com/
 * Description: Content subscription!
 * Author:      Burgeon Software
 * Author URI:  https://burgeonsoftware.net/
 * Version:     1.0
 */

/*
* Creating a function to create our CPT
*/
 
function custom_post_type() {
 
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'News', 'Post Type General Name', 'twentythirteen' ),
        'singular_name'       => _x( 'News', 'Post Type Singular Name', 'twentythirteen' ),
        'menu_name'           => __( 'News', 'twentythirteen' ),
        'parent_item_colon'   => __( 'Parent News', 'twentythirteen' ),
        'all_items'           => __( 'All News', 'twentythirteen' ),
        'view_item'           => __( 'View News', 'twentythirteen' ),
        'add_new_item'        => __( 'Add New News', 'twentythirteen' ),
        'add_new'             => __( 'Add News', 'twentythirteen' ),
        'edit_item'           => __( 'Edit News', 'twentythirteen' ),
        'update_item'         => __( 'Update News', 'twentythirteen' ),
        'search_items'        => __( 'Search News', 'twentythirteen' ),
        'not_found'           => __( 'Not Found', 'twentythirteen' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
    );
     
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => __( 'news', 'twentythirteen' ),
        'description'         => __( 'description', 'twentythirteen' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy. 
        'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
        //This is where we add taxonomies to our CPT
        'taxonomies'          => array( 'newscategories' ),
    );
     
    // Registering your Custom Post Type
    register_post_type( 'news', $args );
 
}
 
/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
 
add_action( 'init', 'custom_post_type');


//hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_topics_hierarchical_taxonomy', 0 );
 
//create a custom taxonomy name it topics for your posts
 
function create_topics_hierarchical_taxonomy() {
 
// Add new taxonomy, make it hierarchical like categories
//first do the translations part for GUI
 
  $labels = array(
    'name' => _x( 'News Categories', 'taxonomy general name' ),
    'singular_name' => _x( 'Category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search News Categories' ),
    'all_items' => __( 'All News Categories' ),
    'parent_item' => __( 'Parent Category' ),
    'parent_item_colon' => __( 'Parent Category:' ),
    'edit_item' => __( 'Edit Category' ), 
    'update_item' => __( 'Update Category' ),
    'add_new_item' => __( 'Add New Category' ),
    'new_item_name' => __( 'New Category Name' ),
    'menu_name' => __( 'News Categories' ),
  );    
 
// Now register the taxonomy
 
  register_taxonomy('newscategories',array('post'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'newscategories' ),
  ));
 
}

add_action( 'custom_cron_schedular_feedme', 'feedMe_callback' );

require_once( plugin_dir_path( __FILE__ ) . 'functions.php' );
require( plugin_dir_path( __FILE__ ) . 'widget.php' );




function cs_install() {
	global $wpdb;
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	$charset_collate = $wpdb->get_charset_collate();

  
	
	$table_name =  "{$wpdb->base_prefix}CS_subscriptions";
	if($wpdb->get_var( "show tables like '$table_name'" ) != $table_name) 
	{
		$sql = "CREATE TABLE $table_name (
			ID bigint(11) NOT NULL AUTO_INCREMENT,
			source varchar(512) NOT NULL, 
			fetchKey varchar(1024) NOT NULL,
			removekey varchar(5024) NOT NULL,
			source_subscription_url varchar(1024) NOT NULL, 
			source_login_url varchar(512) NOT NULL, 
			rss_feeder_url varchar(512) NOT NULL, 
			first_subscribed_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
			nologin tinyint(1),
			ispaid tinyint(1),
			date_created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY  (ID)
		) $charset_collate;";

		dbDelta( $sql );
	}
	
	$table_name =  "{$wpdb->base_prefix}CS_subs_users";
	if($wpdb->get_var( "show tables like '$table_name'" ) != $table_name) 
	{
		$sql = "CREATE TABLE $table_name (
			ID bigint(11) NOT NULL AUTO_INCREMENT,
			subscription_id bigint(11) NOT NULL,
			username varchar(512) NOT NULL, 
			email varchar(1024) NOT NULL, 
			password varchar(512) NOT NULL, 
			date_created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY  (ID)
		) $charset_collate;";

		dbDelta( $sql );
	}
	
	$table_name =  "{$wpdb->base_prefix}CS_subs_card";
	if($wpdb->get_var( "show tables like '$table_name'" ) != $table_name) 
	{
		$sql = "CREATE TABLE $table_name (
			ID bigint(11) NOT NULL AUTO_INCREMENT,
			subscription_id bigint(11) NOT NULL,
			first_name varchar(512) NOT NULL,
			last_name varchar(512) NOT NULL,
			street varchar(512) NOT NULL,
			city varchar(512) NOT NULL,
			state varchar(512) NOT NULL,
			mc varchar(512) NOT NULL,
			cardno varchar(512) NOT NULL,
			exp_m_y varchar(512) NOT NULL,
		    cvc varchar(512) NOT NULL,
			first_subscribed_date varchar(512) NOT NULL,
			current_cost_monthly varchar(512) NOT NULL,
			active tinyint(1) NOT NULL,
			PRIMARY KEY  (ID)
		) $charset_collate;";

		dbDelta( $sql );
	}

	$table_name =  "{$wpdb->base_prefix}User_rating";
	if($wpdb->get_var( "show tables like '$table_name'" ) != $table_name) 
	{
		$sql = "CREATE TABLE $table_name (
			ID bigint(11) NOT NULL AUTO_INCREMENT,
			post_id bigint(11) NOT NULL DEFAULT 0,
			user_id int(11) NOT NULL,
			rate varchar(256) NOT NULL,			
			datetime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY  (ID)
		) $charset_collate;";
		dbDelta( $sql );
	}

}

register_activation_hook( __FILE__, 'cs_install' ); //install tables on activation of plugin



function enqueue_style_ioptn_rating() {

    wp_enqueue_style( 'opin-rating', plugins_url( 'css/opin-rating.css', __FILE__ ), false );

}
add_action( 'wp_enqueue_scripts', 'enqueue_style_ioptn_rating' );



///////////// creating admin area ////////////////////
 


function add_subscriptions() 
{
	global $wpdb;
	$hiddenValue=isset($_GET['type']) && $_GET['type'] == "edit"?"edit_subscription":'add_subscription';
	$getId=isset($_GET['ID'])?$_GET['ID']:'';
	$source_first_subs_date=$source=$subscription_url=$login_url=$rss_feeder_url=$userstring=$card_fname=$card_lname=$card_street=$card_city=$card_state=$card_mc=$card_no=$card_exm=$card_exy=$card_cvv=$card_firstsubs=$card_crntcostmonthly=$card_sts="";
	$ispaid='1';
	$nologin='0';
	if($getId!='')
	{
		$sql = "SELECT * FROM {$wpdb->base_prefix}CS_subscriptions WHERE ID=$getId";
		$result = $wpdb->get_row($sql);
    	$source=$result->source;
    	$subscription_url=$result->source_subscription_url;
    	$fetchKey=$result->fetchKey;
    	$removekey=$result->removekey;
    	$login_url=$result->source_login_url;
    	$rss_feeder_url=$result->rss_feeder_url; 
    	$nologin=$result->nologin; 
    	$ispaid=$result->ispaid;
    	$first_subscribed_date=explode(" ",$result->first_subscribed_date);
    	$source_first_subs_date=$first_subscribed_date[0];
    	if($nologin==0){
    		$usesql="SELECT * FROM {$wpdb->base_prefix}CS_subs_users WHERE subscription_id=$getId";
	    	$userResult = $wpdb->get_results($usesql) or die(mysql_error());
	    	$crosecount=0;
		    foreach( $userResult as $results ) {
		    	if($crosecount==0)
		    	{
		    		$userstring .="<div id='".$results->ID."' class='parentbox'><p>User Name: <input type='text' name='username[]' value='".$results->username."' required>
				</p><p>Email: <input type='email' name='email[]' value='".$results->email."' required></p>
				<p>Password: <input type='password' class='remove_required' name='password[]'></p><input type='hidden' name='update_id[]' value='".$results->ID."'></div>";
		    	}
		    	else
		    	{
		    		$userstring .="<div id='".$results->ID."' class='parentbox'><p>User Name: <input type='text' name='username[]' value='".$results->username."' required>
				</p><p>Email: <input type='email' name='email[]' value='".$results->email."' required></p>
				<p>Password: <input type='password' class='remove_required' name='password[]'></p><a href='javascript:void(0);' class='userboxclose'>Close</a><input type='hidden' name='update_id[]' value='".$results->ID."'> </div>";
		    	}
		    	$crosecount++;
			    
		    }
    	}
    	if($ispaid=='1')
    	{
    		$paysql = "SELECT * FROM {$wpdb->base_prefix}CS_subs_card WHERE subscription_id=$getId";
			$payresult = $wpdb->get_row($paysql);
			$card_fname=$payresult->first_name;
			$card_lname=$payresult->last_name;
			$card_street=$payresult->street;
			$card_city=$payresult->city;
			$card_state=$payresult->state;
			$card_mc=$payresult->mc;
			$card_no=$payresult->cardno;
			$card_exmval=explode("/", $payresult->exp_m_y);
			$card_exm=$card_exmval[0];
			$card_exy=$card_exmval[1];
			$card_cvv=$payresult->cvc;
			$card_firstsubs=$payresult->first_subscribed_date;
			$card_crntcostmonthly=$payresult->current_cost_monthly;
			$card_sts=$payresult->active;
    	}
	    

	}
	
?>
<div class="wrap">
<h2>Add Subscription</h2>
<form method="post" id="add_sub_form" class="single-line-form">
	<table class="form-table">
	<tbody>
		<tr class="form-field form-required">
			<th scope="row"><label for="source">Source: </label></th>
			<td><input id="source" type="text" name="source" value="<?php echo $source; ?>" required></td>
		</tr>
		<tr class="form-field form-required">
			<th scope="row"><label for="subscription_url">Subscription URL: </label></th>
			<td><input id="subscription_url" type="text" name="subscription_url" value="<?php echo $subscription_url; ?>" required></td>
		</tr>
		<tr class="form-field form-required">
			<th scope="row"><label for="fetchKey">Content Fetch Key: </label></th>
			<td><input id="fetchKey" type="text" name="fetchKey" value="<?php echo $fetchKey; ?>" required></td>
		</tr>
		<tr class="form-field form-required">
			<th scope="row"><label for="removekey">Multiple Remove Class/ID Key Separate By "|" :</label></th>
			<td><input id="removekey" type="text" name="removekey" value="<?php echo $removekey; ?>" required></td>
		</tr>
		<tr class="form-field form-required">
			<th scope="row"><label for="login_url">Login URL: </label></th>
			<td><input id="login_url" type="text" name="login_url" value="<?php echo $login_url; ?>" required></td>
		</tr>
		<tr class="form-field form-required">
			<th scope="row"><label for="rss_feeder_url">RSS Feeder URL: </label></th>
			<td><input id="rss_feeder_url" type="text" name="rss_feeder_url" value="<?php echo $rss_feeder_url; ?>" required></td>
		</tr>
		<tr class="form-field form-required">
			<th scope="row"><label for="source_first_subs_date">Source First Subscribed Date: </label></th>
			<td><input id="source_first_subs_date" type="date" value="<?php echo $source_first_subs_date; ?>" name="source_first_subs_date" required></td>
		</tr>
	</tbody>
</table>
		<p>No Login: <input type="checkbox" name="nologin" <?php if($nologin=='1')  { echo 'checked="checked"'; } ?> id="nologin" value="1"></p>
	<div id="loginsec">
	<?php if($nologin!='1' && isset($_GET['type']) && $_GET['type'] == "edit"){ echo $userstring; } else{ ?>
		<div class='parentbox'>
		   <p>User Name: <input type="text" name="username[]" required></p>
		   <p>Email: <input type="email" name="email[]" required></p>
		   <p>Password: <input type="password" name="password[]" required></p>
		   <input type='hidden' name='update_id[]' value='new'>
		</div>
	<?php } ?>
	</div>
	<button type="button" id="adduser" class="button button-primary">Add User</button>
	<p>Paid: <input type="checkbox" name="ispaid" id="ispaid" <?php if($ispaid=='1') { echo 'checked="checked"'; } ?> value="1"></p>
	<div id="paidsec">
	   <table class="form-table">
		<tbody>
	   <tr><th colspan="2"><h4>Billing and Name on Card</h4></th></tr>
	   
		<tr class="form-field form-required">
			<th scope="row"><label for="card_fname">FirstName: </label></th>
			<td><input id="card_fname" type="text" name="card_fname" value="<?php echo $card_fname; ?>" required></td>
		</tr>
	   <tr class="form-field form-required">
			<th scope="row"><label for="card_lname">LastName: </label></th>
			<td><input id="card_lname" type="text" name="card_lname" value="<?php echo $card_lname; ?>" required></td>
		</tr>
       <tr><th colspan="2">Billing Address</th></tr>
       <tr class="form-field form-required">
			<th scope="row"><label for="card_street">Street </label></th>
			<td><input id="card_street" type="text" name="card_street" value="<?php echo $card_lname; ?>" required></td>
		</tr>
       <tr class="form-field form-required">
			<th scope="row"><label for="card_city">City </label></th>
			<td><input id="card_city" type="text" name="card_city" value="<?php echo $card_lname; ?>" required></td>
		</tr>
       <tr class="form-field form-required">
			<th scope="row"><label for="card_state">State </label></th>
			<td><input id="card_state" type="text" name="card_state" value="<?php echo $card_state; ?>" required></td>
		</tr>
       <tr class="form-field form-required">
			<th scope="row"><label for="card_city">MC </label></th>
			<td><input type="text" name="card_mc" value="<?php echo $card_mc; ?>" required></td>
		</tr>
       <tr class="form-field form-required">
			<th scope="row"><label for="card_no">Credit Card No. </label></th>
			<td><input id="card_no" type="number" value="<?php echo $card_no; ?>" name="card_no" required></td>
		</tr>
       <tr class="form-field form-required">
			<th scope="row"><label for="card_no">Expiry Month </label></th>
			<td><input type="number" value="<?php echo $card_exm; ?>" name="card_exm" required></td>
		</tr>
       <tr class="form-field form-required">
			<th scope="row"><label for="card_exy">Expiry Year </label></th>
			<td><input id="card_exy" type="number" value="<?php echo $card_exy; ?>" name="card_exy" required></td>
		</tr>
       <tr class="form-field form-required">
			<th scope="row"><label for="card_cvv">CVC </label></th>
			<td><input type="number" name="card_cvv" value="<?php echo $card_cvv; ?>" required></td>
		</tr>
       <tr class="form-field form-required">
			<th scope="row"><label for="card_firstsubs">First Subscribed </label></th>
			<td><input id="card_firstsubs" type="date" name="card_firstsubs" value="<?php echo $card_firstsubs; ?>" required></td>
		</tr>
       <tr class="form-field form-required">
			<th scope="row"><label for="card_crntcostmonthly">Current Cost Monthly </label></th>
			<td><input id="card_crntcostmonthly" type="number" name="card_crntcostmonthly" value="<?php echo $card_crntcostmonthly; ?>" required></td>
		</tr>
       <tr class="form-field form-required">
			<th scope="row"><label for="card_sts">Active </label></th>
			<td><input id="card_sts" type="radio" name="card_sts" checked value="1" <?php if($card_sts=='1')  { echo 'checked="checked"'; } ?> >Yes <input type="radio" name="card_sts" value="0" <?php if($card_sts=='0')  { echo 'checked="checked"'; } ?>>No</td>
		</tr>
</tbody>
</table>
	</div>
	
	<p><input type="hidden" name="actionManual" value="<?php echo $hiddenValue; ?>">
		<input type="hidden" name="subscription_id" value="<?php echo $getId; ?>">
	</p>
	<p><input type="submit" name="subs_bttn" id="subs_bttn" class="button button-primary"></p>
</form>
</div>
<script>
	jQuery(document).on("click","#add_sub_form #nologin",function(){
		if(jQuery(this).is(":checked"))
		{
			jQuery("#loginsec input").removeAttr("required");
			jQuery("#loginsec").hide();
			jQuery("#adduser").hide();
		}	         
		else
		{
			jQuery("#loginsec").show();
			jQuery("#adduser").show();
			jQuery("#loginsec input").attr("required","required");
			jQuery(".remove_required").removeAttr("required");
		}
  	});
	jQuery(document).ready(function(){


		if(jQuery("#add_sub_form #ispaid").is(":checked"))
		{
			jQuery("#paidsec input").attr("required","required");
			jQuery("#paidsec").show();
		}
		else
		{
			jQuery("#paidsec input").removeAttr("required");
			jQuery("#paidsec").hide();
		}

		if(jQuery("#add_sub_form #nologin").is(":checked"))
		{
			jQuery("#loginsec input").removeAttr("required");
			jQuery("#loginsec").hide();
			jQuery("#adduser").hide();
		}	         
		else
		{
			jQuery("#loginsec").show();
			jQuery("#adduser").show();
			jQuery("#loginsec input").attr("required","required");
		}
		jQuery(".remove_required").removeAttr("required");
		jQuery(document).on("click",".userboxclose",function(){
			jQuery(this).parent().remove();
		});
	});
	jQuery(document).on("click","#add_sub_form #ispaid",function(){
	     if(jQuery(this).is(":checked"))
	     {
			 jQuery("#paidsec input").attr("required","required");
			 jQuery("#paidsec").show();
		 }
	      else
	      {
			  jQuery("#paidsec input").removeAttr("required");
			  jQuery("#paidsec").hide();
		  }
	          
	});

//////////////////// submit form //////////////////
jQuery("#add_sub_form").submit(function(e){
	e.preventDefault();
	var form = jQuery(this);

jQuery.ajax({
	  type: "post",
	  data: form.serialize(),
	  async:false,
	  success: function(res){
		     var json = JSON.parse(res);
		        if(json.result.status == "add_subscription")
		        {
					form.find(".subscription_success").remove();
					form.append("<h3 class='subscription_success' style='color:green'>subscription successfully created!</h3>");
					form.trigger("reset"); 
				}
				else
				{
					form.find(".subscription_success").remove();
					form.append("<h3 class='subscription_success' style='color:green'>subscription successfully updated!</h3>");

					
				}
		  }
	});	
	
});


/////////////////////////////////////////////////
jQuery(document).on("click","#adduser",function(){
	jQuery("#loginsec").append("<div class='parentbox'>\
		<p>User Name: <input type='text' name='username[]' required></p>\
		<p>Email: <input type='email' name='email[]' required></p>\
		<p>Password: <input type='password' name='password[]' required></p>\
		<a href='javascript:void(0);' class='userboxclose'>Close</a>\
		<input type='hidden' name='update_id[]' value='new'></div>");
});
</script>
<?php
}



if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class CS_Customers_List extends WP_List_Table {

	/** Class constructor */
	public function __construct() {

		parent::__construct( [
			'singular' => __( 'Customer', 'sp' ), //singular name of the listed records
			'plural'   => __( 'Customers', 'sp' ), //plural name of the listed records
			'ajax'     => false //should this table support ajax?

		] );
	}

public static function get_customers( $per_page = 5, $page_number = 1 ) {

  global $wpdb;

  $sql = "SELECT * FROM {$wpdb->base_prefix}CS_subscriptions";
  
  $s = trim(@$_REQUEST['s']);
  if($s)
  {
	 $sql .= " WHERE source LIKE '%".$s."%' OR fetchKey LIKE '%".$s."%' OR source_subscription_url LIKE '%".$s."%' OR source_login_url LIKE '%".$s."%' OR first_subscribed_date LIKE '%".$s."%'";  
  }


  if ( ! empty( $_REQUEST['orderby'] ) ) {
    $sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
    $sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
  }

  $sql .= " LIMIT $per_page";

  $sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;


  $result = $wpdb->get_results( $sql, 'ARRAY_A' );

  return $result;
}

public static function delete_customer( $id ) {
  global $wpdb;

  $wpdb->delete(
    "{$wpdb->base_prefix}CS_subscriptions",
    [ 'ID' => $id ],
    [ '%d' ]
  );
}

public static function record_count() {
  global $wpdb;

  $sql = "SELECT COUNT(*) FROM CS_subscriptions";
  $s = trim(@$_REQUEST['s']);
  if($s)
  {
	 $sql .= " WHERE opinword LIKE '%".$s."%' OR action LIKE '%".$s."%' OR value LIKE '%".$s."%' OR lang LIKE '%".$s."%' OR date_added LIKE '%".$s."%'";  
  }

  return $wpdb->get_var( $sql );
}

public function no_items() {
  _e( 'No subscription found.', 'sp' );
}

function column_name( $item ) {

  // create a nonce
  $delete_nonce = wp_create_nonce( 'sp_delete_customer' );

  $title = '<strong>' . $item['name'] . '</strong>';

  $actions = [
    'delete' => sprintf( '<a href="?page=%s&action=%s&customer=%s&_wpnonce=%s">Delete</a>', esc_attr( $_REQUEST['page'] ), 'delete', absint( $item['ID'] ), $delete_nonce )
  ];

  return $title . $this->row_actions( $actions );
}

public function column_default( $item, $column_name ) { 
  switch ( $column_name ) {
    case 'source': 
    case 'fetchKey':
     case 'removekey':
    case 'source_subscription_url':
    case 'source_login_url':
      return $item[ $column_name ];
    default:
      return print_r( $item, true ); //Show the whole array for troubleshooting purposes
  }
}
 

function column_cb( $item ) {
	
	$delete = '<a style="color:red" href="?page='.esc_attr( $_REQUEST['page'] ).'&action=delete&customer='.absint( $item['ID'] ).'&_wpnonce='.wp_create_nonce( 'sp_delete_customer' ).'">Delete</a>';
	
	$edit = '<a style="color:blue" href="admin.php?page=add_subscriptions&type=edit&ID='.$item['ID'].'">Edit</a>';
	
  return sprintf(
   ' <input type="checkbox" name="bulk-delete[]" value="%s" />', $item['ID']
  );
}
function column_source( $item ) {
	$actions = array(
	$edit = sprintf('<a href="admin.php?page=add_subscriptions&type=edit&ID='.$item['ID'].'">Edit</a>'),
	$delete => sprintf('<a style="color:red" href="?page='.esc_attr( $_REQUEST['page'] ).'&action=delete&customer='.absint( $item['ID'] ).'&_wpnonce='.wp_create_nonce( 'sp_delete_customer' ).'">Delete</a>'),
	);
  return sprintf( '%1$s %2$s', '<a style="font-size:14px;" href="admin.php?page=add_subscriptions&type=edit&ID='.$item['ID'].'">'.$item['source'].'</a>', $this->row_actions($actions) );
}


function get_columns() {
  $columns = [
    'cb'      => '<input type="checkbox" />',
    'source'    => __( 'Source', 'sp' ),
    'fetchKey' => __( 'Content Fatch Key', 'sp' ),
    'source_subscription_url' => __( 'Subscription URL', 'sp' ),
    'source_login_url'    => __( 'Login URL', 'sp' )
  ];

  return $columns;
}

public function get_sortable_columns() {
  $sortable_columns = array(
    'source' => array( 'source', true )
  );

  return $sortable_columns;
}

public function get_bulk_actions() {
  $actions = [
    'bulk-delete' => 'Delete'
  ];

  return $actions;
}

public function prepare_items() {

  $this->_column_headers = $this->get_column_info();

  /** Process bulk action */
  $this->process_bulk_action();

  $per_page     = $this->get_items_per_page( 'customers_per_page', 5 );
  $current_page = $this->get_pagenum();
  $total_items  = self::record_count();

  $this->set_pagination_args( [
    'total_items' => $total_items, //WE have to calculate the total number of items
    'per_page'    => $per_page //WE have to determine how many items to show on a page
  ] );


  $this->items = self::get_customers( $per_page, $current_page );
}

public function process_bulk_action() {

  //Detect when a bulk action is being triggered...
  if ( 'delete' === $this->current_action() ) {

    // In our file that handles the request, verify the nonce.
    $nonce = esc_attr( $_REQUEST['_wpnonce'] );

    if ( ! wp_verify_nonce( $nonce, 'sp_delete_customer' ) ) {
      die( 'Go get a life script kiddies' );
    }
    else {
      self::delete_customer( absint( $_GET['customer'] ) );

      //wp_redirect( esc_url( add_query_arg() ) );
      //exit;
    }

  }

  // If the delete bulk action is triggered
  if ( ( isset( $_POST['action'] ) && $_POST['action'] == 'bulk-delete' )
       || ( isset( $_POST['action2'] ) && $_POST['action2'] == 'bulk-delete' )
  ) {

    $delete_ids = esc_sql( $_POST['bulk-delete'] );

    // loop over the array of record IDs and delete them
    foreach ( $delete_ids as $id ) {
      self::delete_customer( $id );

    }

    //wp_redirect( esc_url( add_query_arg() ) );
    //exit;
  }
}

}// end class

class CS_Plugin {

	// class instance
	static $instance;

	// customer WP_List_Table object
	public $customers_obj;

	// class constructor
	public function __construct() {
		add_filter( 'set-screen-option', [ __CLASS__, 'set_screen' ], 10, 3 );
		add_action( 'admin_menu', [ $this, 'plugin_menu' ] );
	}

public static function set_screen( $status, $option, $value ) {
	return $value;
}

public function plugin_menu() {
$logo = plugin_dir_url( __FILE__ )."images/fav_logo.png";
$page_ttl = 'Content Subscription';
$plugin_ttl = 'Subscription';
$slug = 'content_subscriptions';



	$hook = add_menu_page(
		$page_ttl,
		$plugin_ttl,
		'manage_options',
		$slug,
		[ $this, 'plugin_settings_page' ],
		$logo
	);
	
	
	add_submenu_page( $slug, 'OpinFacts | Opinwords', 'Add Subscription', 'manage_options', 'add_subscriptions', 'add_subscriptions');

	add_action( "load-$hook", [ $this, 'screen_option' ] );

}

public function screen_option() {

	$option = 'per_page';
	$args   = [
		'label'   => 'Words',
		'default' => 5,
		'option'  => 'customers_per_page'
	];

	add_screen_option( $option, $args );

	$this->customers_obj = new CS_Customers_List();
}

public function plugin_settings_page() {
	?>
	<div class="wrap">
<a class="wf-btn wf-btn-default wf-btn-sm" href="admin.php?page=add_subscriptions" >Add subscription</a>
	
		<div id="poststuff">
			<div id="post-body" class="metabox-holder columns-3">
<h2><u>Subscriptions</u></h2>
				<div id="post-body-content">
					<div class="meta-box-sortables ui-sortable">
						<form method="post" style="float:right">
						  <input type="text" name="s" placeholder="Search" value="<?php echo @$_REQUEST['s']; ?>"> 
						<input type="submit" value="Search"> 
						</form>
						<form method="post">
							<?php
							$this->customers_obj->prepare_items();
							$this->customers_obj->display(); ?>
						</form>
					</div>
				</div>
			</div>
			<br class="clear">
		</div>
	</div>
<?php
}

public static function get_instance() {
	if ( ! isset( self::$instance ) ) {
		self::$instance = new self();
	}

	return self::$instance;
}


} //end class

add_action( 'plugins_loaded', function () {
	CS_Plugin::get_instance();
} );

//news Feed function
function adjCounter($adjective_count,$adCount,$count)
{
	$adjective_count=$adjective_count+$adCount*$count;
	return $adjective_count;
}

function feedMeFunction($feed,$subsurl,$fetchKey,$source,$removekey) {

	$PostidArr=array();
	global $wpdb;
	$user='jl2020m@gmail.com';
	$pass='risky123';
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $feed);
	curl_setopt($curl, CURLOPT_USERPWD, "$user:$pass");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt ($ch, CURLOPT_USERAGENT, $useragent);
	$rss = curl_exec($ch);
	curl_close($ch);
	$rss = simplexml_load_string($rss);

	/*echo "<pre>";print_r($rss);echo "</pre>";*/
	$html="";

	$getresult = $wpdb->get_results("select * from {$wpdb->base_prefix}OF_words") or die(mysql_error());
	$getcat = $wpdb->get_results("select * from {$wpdb->base_prefix}KEY_words") or die(mysql_error());
	/*echo "<pre>";print_r($getcat);echo "</pre>";*/
	$siteTitle = $rss->channel->title;
	$siteLink = $rss->channel->link;
	$html .= "<h1>".$siteTitle."</h1>";
	$cnt = count($rss->channel->item);
	
	require_once(ABSPATH . 'wp-admin/includes/image.php');
	
	for($i=0; $i<$cnt; $i++) {
		$url = isset($rss->channel->item[$i]->link)?$rss->channel->item[$i]->link:'Link';
		/*echo $url."</br>";*/
		$title = isset($rss->channel->item[$i]->title)?$rss->channel->item[$i]->title:'Title';
		$raisedcont1 ="<h1 class='news-title'>".$title."</h1>";
		$file=file_get_contents($url);
		$doc = new DOMDocument();
		@$doc->loadHTML($file);
		$raisedcont = $doc->getElementById($fetchKey);
		
		
		$expoldeclass=explode("|",$removekey);
		$selector = new DOMXPath($doc);
		foreach ($expoldeclass as $classsis) {
			if($classsis)
			{
				$div=$doc->getElementById(trim($classsis));
			    if( $div && $div->nodeType==XML_ELEMENT_NODE ){
			        $div->parentNode->removeChild( $div );
			    }
				foreach($selector->query('//div[contains(attribute::class, "'.trim($classsis).'")]') as $e) 
				{
					$e->parentNode->removeChild($e);
				}
				foreach($selector->query('//ul[contains(attribute::class, "'.trim($classsis).'")]') as $e) 
				{
					$e->parentNode->removeChild($e);
				}
				foreach($selector->query('//article[contains(attribute::class, "'.trim($classsis).'")]') as $e)
				{
					$e->parentNode->removeChild($e);
				}
				foreach($selector->query('//section[contains(attribute::class, "'.trim($classsis).'")]') as $e)
				{
					$e->parentNode->removeChild($e);
				}
				foreach($selector->query('//span[contains(attribute::class, "'.trim($classsis).'")]') as $e)
				{
					$e->parentNode->removeChild($e);
				}
				foreach($selector->query('//h1[contains(attribute::class, "'.trim($classsis).'")]') as $e)
				{
					$e->parentNode->removeChild($e);
				}
				foreach($selector->query('//*[contains(attribute::class, "'.trim($classsis).'")]') as $e)
				{
					$e->parentNode->removeChild($e);
				}
			}
			
		}
		
		
		
		
		/*image upload*/		
		$images = $doc->getElementsByTagName('img');
		$uploaddir = wp_upload_dir();
		
$allowedTypes = array('jpg','jpeg','png');
$todaydate=current_time('Y-m-d');
$checktitle = $wpdb->get_row("SELECT `ID` FROM {$wpdb->base_prefix}posts WHERE `post_title`='$title' AND `post_date` LIKE '$todaydate%'");	
		if(!isset($checktitle->ID)) {
		foreach ($images as $image) {
			$img = $image->getAttribute('src');			 			 	
			
			$name = pathinfo(parse_url($img)['path'], PATHINFO_FILENAME);
			$ext = pathinfo(parse_url($img)['path'], PATHINFO_EXTENSION);
			
			if(in_array($ext,$allowedTypes))
			{		
										
		
		
			$imgname = $name.'.'.$ext;
			
			
			if (wp_mkdir_p($uploaddir['path']))
 $uploadfile = $uploaddir['path'] . '/'.$imgname;
 else
 $uploadfile = $uploaddir['basedir'] . '/'.$imgname;

			
			 
			 $uploaded_url =  $uploaddir['url'].'/'.$imgname;			 			 
			 
$contents= file_get_contents($img);
$savefile = fopen($uploadfile, 'w');
fwrite($savefile, $contents);
fclose($savefile); 
												
																		
				
$wp_filetype = wp_check_filetype($imgname, null);

 $attachment = array(
 'post_mime_type' => $wp_filetype['type'],
 'post_title' => sanitize_file_name($imgname),
 'post_content' => '',
 'post_status' => 'inherit'
 );

$attach_id = wp_insert_attachment( $attachment, date("Y/m/").$imgname );

 $attach_data = wp_generate_attachment_metadata($attach_id, $uploadfile);
 wp_update_attachment_metadata($attach_id, $attach_data);
				
				
		$image->setAttribute('src', wp_get_attachment_url( $attach_id ));
		
														
				
		    }
																							
		}
	}	

		  $raisedcont = $doc->saveHTML($raisedcont);
				
				
						
		//$raisedcont= $raisedcont1.$raisedcont;
		$raisedcont = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $raisedcont);
		$raisedcont = preg_replace('#<style(.*?)>(.*?)</style>#is', '', $raisedcont);
		$raisedcont=preg_replace('/<\/?a[^>]*>/','',$raisedcont);								
									
			
		$desc = isset($rss->channel->item[$i]->description)?$rss->channel->item[$i]->description:'Description';
		$rss->channel->item[$i]->description = isset($rss->channel->item[$i]->description)?$title:'Description';
		$author = @$rss->channel->item[$i]->children('dc', TRUE);
		if($author=='')
		{
			$author=isset($rss->channel->item[$i]->author)?$rss->channel->item[$i]->author:'';
		}
		if($author=='')
			$rss->channel->item[$i]->author=strip_tags($source);
		else
		$rss->channel->item[$i]->author=strip_tags($author.', '.$source);
		$replace_with=$delete_word="";
		$adjective_count=0;
		foreach ($getresult as $rRow) {
			$rOpinword=$rRow->opinword;
			$adjvalue=$rRow->value;
			$adCount=0;
			if($adjvalue=='positive')
			{
				$adCount=1;
			}
			elseif ($adjvalue=='negative') {
				$adCount=-1;
			}
			$rOpinword1=" ".$rOpinword." ";
			$rOpinword3=" ".$rOpinword.".";
			$rOpinword2=" ".$rOpinword.",";
			if($rRow->action=="replace")
			{
				$nReplace_with=$rRow->replace_with;
				$raisedcont = str_ireplace($rOpinword1,"<span class='replace_word'>".$nReplace_with."</span>",$raisedcont,$count1);
				$raisedcont = str_ireplace($rOpinword3,"<span class='replace_word'>".$nReplace_with."</span>",$raisedcont,$count3);
				$raisedcont = str_ireplace($rOpinword2,"<span class='replace_word'>".$nReplace_with."</span>",$raisedcont,$count4);
				$adjective_count=adjCounter($adjective_count,$adCount,$count1);
				$adjective_count=adjCounter($adjective_count,$adCount,$count3);
				$adjective_count=adjCounter($adjective_count,$adCount,$count4);
				if($count4>0 || $count3>0  || $count1>0)
				{
					if($replace_with=="")
					{
						$replace_with=$rOpinword;
					}
					else
					{
						$replace_with=$replace_with.','.$rOpinword;
					}
				}
			}
			elseif($rRow->action=="delete")
			{
				$raisedcont = str_ireplace($rOpinword1,"<span class='remove_word'> </span>",$raisedcont,$count1);
				$raisedcont = str_ireplace($rOpinword2,"<span class='remove_word'> </span>",$raisedcont,$count2);
				$raisedcont = str_ireplace($rOpinword3,"<span class='remove_word'> </span>",$raisedcont,$count3);
				$adjective_count=adjCounter($adjective_count,$adCount,$count1);
				$adjective_count=adjCounter($adjective_count,$adCount,$count2);
				$adjective_count=adjCounter($adjective_count,$adCount,$count3);
				if($count3>0 || $count2>0 || $count1>0)
				{
					if($delete_word=="")
					{
						$delete_word=$rOpinword;
					}
					else
					{
						$delete_word=$delete_word.','.$rOpinword;
					}
				}
			}
			elseif($rRow->action=="font_color")
			{
				$nReplace_with=$rRow->replace_with;
				$raisedcont = str_ireplace($rOpinword1,"<span style='color:".$nReplace_with.";'>$rOpinword1</span>",$raisedcont,$count1);
				$raisedcont = str_ireplace($rOpinword2,"<span style='color:".$nReplace_with.";'>$rOpinword2</span>",$raisedcont,$count2);
				$raisedcont = str_ireplace($rOpinword3,"<span style='color:".$nReplace_with.";'>$rOpinword3</span>",$raisedcont,$count3);
				$adjective_count=adjCounter($adjective_count,$adCount,$count1);
				$adjective_count=adjCounter($adjective_count,$adCount,$count2);
				$adjective_count=adjCounter($adjective_count,$adCount,$count3);
			}
			elseif($rRow->action=="font")
			{
				$nReplace_with=json_decode($rRow->replace_with);
				if($nReplace_with->font!="Default Font" && $nReplace_with->font!="")
				{
					$fontFamily=$nReplace_with->font;
					$raisedcont = str_ireplace($rOpinword1,"<span style='font-family:".$fontFamily.";'>$rOpinword1</span>",$raisedcont,$count1);
					$raisedcont = str_ireplace($rOpinword2,"<span style='font-family:".$fontFamily.";'>$rOpinword2</span>",$raisedcont,$count2);
					$raisedcont = str_ireplace($rOpinword3,"<span style='font-family:".$fontFamily.";'>$rOpinword3</span>",$raisedcont,$count3);
					$adjective_count=adjCounter($adjective_count,$adCount,$count1);
					$adjective_count=adjCounter($adjective_count,$adCount,$count2);
					$adjective_count=adjCounter($adjective_count,$adCount,$count3);
				}
				if($nReplace_with->bold!="")
				{
				$raisedcont = str_ireplace($rOpinword1,"<strong>$rOpinword1</strong>",$raisedcont,$count1);
				$raisedcont = str_ireplace($rOpinword2,"<strong>$rOpinword2</strong>",$raisedcont,$count2);
				$raisedcont = str_ireplace($rOpinword3,"<strong>$rOpinword3</strong>",$raisedcont,$count3);
				$adjective_count=adjCounter($adjective_count,$adCount,$count1);
					$adjective_count=adjCounter($adjective_count,$adCount,$count2);
					$adjective_count=adjCounter($adjective_count,$adCount,$count3);
				}
				if($nReplace_with->italic!="")
				{
					$raisedcont = str_ireplace($rOpinword1,"<em>$rOpinword1</em>",$raisedcont,$count1);
					$raisedcont = str_ireplace($rOpinword2,"<em>$rOpinword2</em>",$raisedcont,$count2);
					$raisedcont = str_ireplace($rOpinword3,"<em>$rOpinword3</em>",$raisedcont,$count3);
					$adjective_count=adjCounter($adjective_count,$adCount,$count1);
					$adjective_count=adjCounter($adjective_count,$adCount,$count2);
					$adjective_count=adjCounter($adjective_count,$adCount,$count3);
				}
				if($nReplace_with->font_size!="")
				{
					$fontSizecustome=$nReplace_with->font_size;
					$raisedcont = str_ireplace($rOpinword1,"<span style='font-size:".$fontSizecustome.";'>$rOpinword1</span>",$raisedcont,$count1);
					$raisedcont = str_ireplace($rOpinword2,"<span style='font-size:".$fontSizecustome.";'>$rOpinword2</span>",$raisedcont,$count2);
					$raisedcont = str_ireplace($rOpinword3,"<span style='font-size:".$fontSizecustome.";'>$rOpinword3</span>",$raisedcont,$count3);
					$adjective_count=adjCounter($adjective_count,$adCount,$count1);
					$adjective_count=adjCounter($adjective_count,$adCount,$count2);
					$adjective_count=adjCounter($adjective_count,$adCount,$count3);
				}
				if($nReplace_with->strikethrough!="")
				{
					$raisedcont = str_ireplace($rOpinword1,"<del>$rOpinword1</del>",$raisedcont,$count1);
					$raisedcont = str_ireplace($rOpinword2,"<del>$rOpinword2</del>",$raisedcont,$count2);
					$raisedcont = str_ireplace($rOpinword3,"<del>$rOpinword3</del>",$raisedcont,$count3);
					$adjective_count=adjCounter($adjective_count,$adCount,$count1);
					$adjective_count=adjCounter($adjective_count,$adCount,$count2);
					$adjective_count=adjCounter($adjective_count,$adCount,$count3);
					
				}
				
			}
		}
		$currentupdate=current_time( 'mysql' ); 
		$todaydate=current_time('Y-m-d');
		$user_id = get_current_user_id();
		$dataArr = array( 
				'post_date' => $currentupdate,
				'post_title' => $title,
				'post_excerpt' => json_encode($rss->channel->item[$i]),
				'post_content' => $raisedcont,
				'post_type' => 'news', 
				'post_status' => 'publish',
				'ping_status' => 'open',
			);
		
		$checktitle = $wpdb->get_results("SELECT `post_title` FROM {$wpdb->base_prefix}posts WHERE `post_title`='$title' AND `post_date` LIKE '$todaydate%'");
		$rowcount = $wpdb->num_rows;
		$id1=0;
		if($rowcount<1)
		{
			if($siteTitle!='NYT > Americas' || $author!='')
			{
				remove_filter('content_save_pre', 'wp_filter_post_kses');
				remove_filter('content_filtered_save_pre', 'wp_filter_post_kses');
				$id1 = wp_insert_post( $dataArr );
				add_filter('content_save_pre', 'wp_filter_post_kses');
				add_filter('content_filtered_save_pre', 'wp_filter_post_kses');
			}
		}
		
		$termsarray=array();
		if($id1)
		{
			$added2 = add_metadata( 'post', $id1, 'delete_word', $delete_word);
			$added2 = add_metadata( 'post', $id1, 'replace_with', $replace_with);
			$Liberal=$Conservative=0;
			foreach ($getcat as $row1) {
				$count1=$count2=$count3=0;
				$uword=$lword=$cword="";
				$uword=" ".strtoupper($row1->keyword)." ";
				$lword=" ".strtolower($row1->keyword)." ";
				$cword=" ".ucwords($row1->keyword)." ";
				$count1=substr_count($raisedcont,$uword);
				$count2=substr_count($raisedcont,$lword);
				$count3=substr_count($raisedcont,$cword);
				if($count1>0 || $count2>0 || $count3>0)
				{
						
					/*if($row1->keyword=='Football')
					{
						echo $uword.','.$lword.','.$cword."</br>";
						echo $row1->keyword."</br>";
						echo $count1.','.$count2.','.$count3."</br>";
					}*/
					$cct=$wpdb->get_row("SELECT `name` FROM {$wpdb->base_prefix}terms WHERE `term_id`=".$row1->key_type);
					$termsarray[]=$cct->name;
					$added = add_metadata( 'post', $id1, 'subject_key', $row1->ID);
					if($row1->rating_type=='Conservative')
					{
						$Conservative++;
					}
					elseif($row1->rating_type=='Liberal')
					{
						$Liberal++;
					}
				}
			}
			
			if(!empty($termsarray))
			{
				$taxonomy = 'newscategories';
				wp_set_object_terms($id1, $termsarray, $taxonomy);
			}
			else
			{
				$PostidArr[]=$id1;
				
			}
			$added1 = add_metadata( 'post', $id1, 'source_subscription_url', $subsurl);
			$added2 = add_metadata( 'post', $id1, 'news_source', $source);
			$added1 = add_metadata( 'post', $id1, 'adjective_count', $adjective_count);
			if($Liberal>=$Conservative && $Liberal!=0)
			{
				$addedtype1 = add_metadata( 'post', $id1, 'post_subject_type', 'liberal');
				if($adjective_count>0)
				{
					$addedtype1 = add_metadata( 'post', $id1, 'rating_type', 'liberal');
				}
				elseif($adjective_count<0)
				{
					$addedtype1 = add_metadata( 'post', $id1, 'rating_type', 'conservative');
				}
				else
				{
					$addedtype1 = add_metadata( 'post', $id1, 'rating_type', 'moderate');
				}

			}
			elseif($Liberal<$Conservative && $Conservative!=0)
			{
				$addedtype2 = add_metadata( 'post', $id1, 'post_subject_type', 'conservative');
				if($adjective_count>0)
				{
					$addedtype1 = add_metadata( 'post', $id1, 'rating_type', 'conservative');
				}
				elseif($adjective_count<0)
				{
					$addedtype1 = add_metadata( 'post', $id1, 'rating_type', 'liberal');
				}
				else
				{
					$addedtype1 = add_metadata( 'post', $id1, 'rating_type', 'moderate');
				}
			}
			else
			{
				$addedtype3 = add_metadata( 'post', $id1, 'post_subject_type', 'non-political');
				if($adjective_count>=0)
				{
					$addedtype1 = add_metadata( 'post', $id1, 'rating_type', 'un-biased');
				}
				elseif($adjective_count<0)
				{
					$addedtype1 = add_metadata( 'post', $id1, 'rating_type', 'biased');
				}
				else
				{
					$addedtype1 = add_metadata( 'post', $id1, 'rating_type', '');
				}
			}
		}
	}
	if(!empty($PostidArr))
	{
		$to = get_bloginfo("admin_email");
		$subject = 'Article missing Keyword';
		$message = '<html>
<head>
<meta charset="utf-8">
<title>Invite Others To Take CFRP</title>
</head>
<body>Following link have not any category</br> ';
		foreach ($PostidArr as $value) {
			
			$message .="<p> POST Title:".get_the_title( $value )."  POST ID:".$value."</p>";
			wp_trash_post((int)$value);

		}
		$message .='</body>
</html>';
		 $header = "MIME-Version: 1.0\n";
    $header .= "Content-Type: text/html; charset=utf-8\n";
    $header .= "From: IoptN.com " . "<".$to.">";
		
		if(wp_mail( $to, $subject, $message, $header ))
			echo "success";
		else
			echo "Fail";
	}

}

function feedMe_callback() {
	global $wpdb;
	$qry = "UPDATE {$wpdb->base_prefix}KEY_words SET pre_rank=100,curr_rank=100,curr_status='up'";
	$wpdb->query($wpdb->prepare($qry,1));
	$get_url = "SELECT * FROM {$wpdb->base_prefix}CS_subscriptions WHERE 1 ORDER BY ID DESC";
	$urlResult = $wpdb->get_results($get_url) or die(mysql_error());
	$html="";
	//include('simple_html_dom.php');
	foreach( $urlResult as $result ) {
		feedMeFunction($result->rss_feeder_url,$result->source_subscription_url,$result->fetchKey,$result->source,$result->removekey);
		//usleep(500);
	}
	
}

add_shortcode('feedMe', 'feedMe_callback');

//show news listing

function showMeNews()
{
	global $wpdb;
	$tt=current_time('mysql');
		$currentDate=current_time('Y-m-d');
		$yesterdayno=1;
	$getresult = $wpdb->get_results("SELECT * FROM {$wpdb->base_prefix}posts WHERE post_date like '$currentDate%' AND post_type='news' AND post_status='publish' ORDER BY ID DESC");
	$countPost=$wpdb->num_rows;
	
	if($countPost<1)
	{
		$currentDate=date('Y-m-d', strtotime("-$yesterdayno day", strtotime(current_time('Y-m-d'))));
		$getresult = $wpdb->get_results("SELECT * FROM {$wpdb->base_prefix}posts WHERE post_date like '$currentDate%' AND post_type='news' AND post_status='publish'");
		$yesterdayno=2;
	}
	$html="<div class='news-box'><div class='news-head'><h2><span class='big'>Home</span> - Today</h2> <div class='cat-filter'>
				<form class='filter-form'>
					<div class='form-group form-group-sm'>
						<label for='cat-filt'>Filter: </label>
						<select id='cat-filt' class='selectpicker filt_by' name='filt_by' autocomplete='off'>
							<option selected value=''>All</option>
							<option value='liberal'>Political- Liberal</option>
							<option value='moderate'>Political- Moderate</option>
							<option value='conservative'>Political- Conservative</option>
							<option value='biased'>Non-Political- Biased</option>
							<option value='un-biased'>Non-Political- Un-Biased</option>
						</select>
					</div>
				</form>
			</div>
			</div>
	<div class='box-wrap'><div id='append_today'><ul>";
	foreach ($getresult as $row) {
		$getrating = $wpdb->get_row("SELECT `meta_value` FROM {$wpdb->base_prefix}postmeta WHERE `post_id`='".$row->ID."' AND `meta_key`='rating_type'");
		$showrating=$getrating->meta_value;
		$spanvalue="<span class='rate black'>&nbsp;</span><span class='rate green'>&nbsp;</span>";
		if($showrating=='un-biased')
			$spanvalue="<span class='rate black'>&nbsp;</span><span class='rate green active'>&nbsp;</span>";
		elseif($showrating=='biased')
			$spanvalue="<span class='rate black active'>&nbsp;</span><span class='rate green'>&nbsp;</span>";
		$term_obj_list = get_the_terms( $row->ID, 'newscategories' );
		if(!empty($term_obj_list))
		foreach ($term_obj_list as $loop) {
			if($loop->name=="Politics")
			{
				$spanvalue="<span class='rate blue'>&nbsp;</span><span class='rate white'>&nbsp;</span><span class='rate red'>&nbsp;</span>";
				if($showrating=='liberal')
					$spanvalue="<span class='rate blue active'>&nbsp;</span><span class='rate white'>&nbsp;</span><span class='rate red'>&nbsp;</span>";
				elseif ($showrating=='moderate')
					$spanvalue="<span class='rate blue'>&nbsp;</span><span class='rate white active'>&nbsp;</span><span class='rate red'>&nbsp;</span>";
				elseif ($showrating=='conservative')
					$spanvalue="<span class='rate blue'>&nbsp;</span><span class='rate white'>&nbsp;</span><span class='rate red active'>&nbsp;</span>";
			}
		}
		$jdata=json_decode($row->post_excerpt);
		/*echo "<pre>";print_r($jdata);echo "</pre>";*/
		$html .="<li id='".$row->ID."' class='readContent'><span class='rate-group'>".$spanvalue."</span><a href='".get_permalink( $row->ID )."' target='_blank'>".@$row->post_title."</a><span class='auth-name ".$showrating."-text'>".@$jdata->author."</span></li>";
	}
	$html .="</ul></div></div></div>";
	$pDate=date('Y-m-d', strtotime("-$yesterdayno day", strtotime(current_time('Y-m-d'))));
	$html .="<div class='news-box'><h2>".date("M", strtotime($pDate))." ".date("d", strtotime($pDate)).", ".date("Y", strtotime($pDate))."</h2>";
	$getresultpre = $wpdb->get_results("SELECT * FROM {$wpdb->base_prefix}posts WHERE post_date like '$pDate%' AND post_type='news' AND post_status='publish'");
	$html .="<div class='box-wrap'><div id='append_yesterday'><ul>";
	foreach ($getresultpre as $row) {
		$getrating = $wpdb->get_row("SELECT `meta_value` FROM {$wpdb->base_prefix}postmeta WHERE `post_id`='".$row->ID."' AND `meta_key`='rating_type'");
		$showrating=$getrating->meta_value;
		$spanvalue="<span class='rate black'>&nbsp;</span><span class='rate green'>&nbsp;</span>";
		if($showrating=='un-biased')
			$spanvalue="<span class='rate black'>&nbsp;</span><span class='rate green active'>&nbsp;</span>";
		elseif($showrating=='biased')
			$spanvalue="<span class='rate black active'>&nbsp;</span><span class='rate green'>&nbsp;</span>";
		$term_obj_list = get_the_terms( $row->ID, 'newscategories' );
		if(!empty($term_obj_list))
		foreach ($term_obj_list as $loop) {
			if($loop->name=="Politics")
			{
				$spanvalue="<span class='rate blue'>&nbsp;</span><span class='rate white'>&nbsp;</span><span class='rate red'>&nbsp;</span>";
				if($showrating=='liberal')
					$spanvalue="<span class='rate blue active'>&nbsp;</span><span class='rate white'>&nbsp;</span><span class='rate red'>&nbsp;</span>";
				elseif ($showrating=='moderate')
					$spanvalue="<span class='rate blue'>&nbsp;</span><span class='rate white active'>&nbsp;</span><span class='rate red'>&nbsp;</span>";
				elseif ($showrating=='conservative')
					$spanvalue="<span class='rate blue'>&nbsp;</span><span class='rate white'>&nbsp;</span><span class='rate red active'>&nbsp;</span>";
			}
		}
		$jdata=json_decode($row->post_excerpt);
		
		$html .="<li id='".$row->ID."' class='readContent'><span class='rate-group'>".$spanvalue."</span><a href='".get_permalink( $row->ID )."' target='_blank'>".@$row->post_title."</a><span class='auth-name ".$showrating."-text'>".@$jdata->author."</span></li>";
	}
	$html .="</ul></div></div></div>";

	echo $html;
	?>
	<script>
		jQuery(document).ready(function(){
			jQuery(document).on('change','#cat-filt',function(){
				var rating_type = jQuery(this).val();
				var url = "<?php echo admin_url('admin-ajax.php'); ?>";
				var data = {
                	'action': "today_rating_filter",
                	'rating_type': rating_type,
                	'dateType': "<php echo $yesterdayno; ?>"
                };
	            jQuery.post(url, data, function(response) {
	            	/*console.log(response);*/
	            	var json = jQuery.parseJSON(response);
	            	jQuery('#append_today').html(json.today);
	            	jQuery('#append_yesterday').html(json.yesterday);
	            })
			});
		});
	</script>
	<?php
}

add_shortcode('showMe','showMeNews');

