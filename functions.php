<?php

if(isset($_POST['actionManual'])){
	global $wpdb;
	$action = @$_POST['actionManual'];
	$data = array();
	$size=0;
	if(isset($_POST['username']))
	{
		$size = sizeof($_POST['username']);
	}
	switch($action)
	{
		case "add_subscription":		
			$source = @$_POST['source'];
			$fetchKey = @$_POST['fetchKey'];
			$removekey = @$_POST['removekey'];
			$subscription_url = @$_POST['subscription_url'];
			$login_url = @$_POST['login_url'];
			$rss_feeder_url = @$_POST['rss_feeder_url'];
			$source_first_subs_date = @$_POST['source_first_subs_date'];
			$nologin = (int)@$_POST['nologin'];
			$ispaid = (int)@$_POST['ispaid'];
			$tblname = "{$wpdb->base_prefix}CS_subscriptions";
		   	$dataArr = array( 
				'source' => addslashes($source),
				'fetchKey' => $fetchKey,
				'removekey' => $removekey,
				'source_subscription_url' => $subscription_url,
				'source_login_url' => $login_url,
				'rss_feeder_url' => $rss_feeder_url,
				'first_subscribed_date' => $source_first_subs_date,
				'nologin' => $nologin,
				'ispaid' => $ispaid,
			);
						   
			$inserted = $wpdb->insert($tblname, $dataArr); 
			$subscription_id = $wpdb->insert_id;
			
			if(!$nologin)
			{
				$tblname = "{$wpdb->base_prefix}CS_subs_users";
				$username = @$_POST['username'];
				$email = @$_POST['email'];
				$password = @$_POST['password'];	
				for ($i=0; $i < $size; $i++) { 
					$dataArr = array( 
					'subscription_id' => $subscription_id,
					'username' => $username[$i],
					'email' => $email[$i],
					'password' => MD5($password[$i]),
					);					   
				 	$inserted = $wpdb->insert($tblname, $dataArr); 
				}
				
			}
			
			if($ispaid)
			{
				$card_fname = @$_POST['card_fname'];
				$card_lname = @$_POST['card_lname'];
				$card_street = @$_POST['card_street'];
				$card_city = @$_POST['card_city'];
				$card_state = @$_POST['card_state'];
				$card_mc = @$_POST['card_mc'];
				$card_no = @$_POST['card_no'];
				$card_exm = @$_POST['card_exm'];
				$card_exy = @$_POST['card_exy'];
				$card_cvv = @$_POST['card_cvv'];
				$card_firstsubs = @$_POST['card_firstsubs'];
				$card_crntcostmonthly = @$_POST['card_crntcostmonthly'];
				$card_sts = @$_POST['card_sts'];	
				
				$tblname = "{$wpdb->base_prefix}CS_subs_card";		
			   	$dataArr = array( 
					'subscription_id' => $subscription_id,
					'first_name' => $card_fname,
					'last_name' => $card_lname,
					'street' => $card_street,
					'city' => $card_city,
					'state' => $card_state,
					'mc' => $card_mc,
					'cardno' => $card_no,
					'exp_m_y' => $card_exm."/".$card_exy,
					'cvc' => $card_cvv,
					'first_subscribed_date' => $card_firstsubs,
					'current_cost_monthly' => $card_crntcostmonthly,
					'active' => $card_sts,

				);					   
			   $inserted = $wpdb->insert($tblname, $dataArr); 
			}
					
			
			$data['result']['status'] = $_POST['actionManual'];
			
			echo json_encode($data);
		break;

		case "edit_subscription":		
			$source = @$_POST['source'];
			$subscription_url = @$_POST['subscription_url'];
			$fetchKey = @$_POST['fetchKey'];
			$removekey = @$_POST['removekey'];
			$login_url = @$_POST['login_url'];
			$rss_feeder_url = @$_POST['rss_feeder_url'];
			$source_first_subs_date = @$_POST['source_first_subs_date'];
			$nologin = (int)@$_POST['nologin'];
			$ispaid = (int)@$_POST['ispaid'];
			$subscription_id = (int)@$_POST['subscription_id'];
			$tblname = "{$wpdb->base_prefix}CS_subscriptions";
			$inserted=$wpdb->query($wpdb->prepare("UPDATE $tblname SET 
				source='".addslashes($source)."',
				fetchKey='".$fetchKey."',
				removekey='".$removekey."',
				source_subscription_url='".$subscription_url."',
				source_login_url='".$login_url."',
				rss_feeder_url='".$rss_feeder_url."',
				first_subscribed_date='".$source_first_subs_date."',
				nologin='".$nologin."',
				ispaid='".$ispaid."'
				WHERE ID =".$subscription_id,1));	
			if(!$nologin)
			{

				$tblname = "{$wpdb->base_prefix}CS_subs_users";
				/*echo json_encode(@$_POST['update_id']);*/
				$username = @$_POST['username'];
				$email = @$_POST['email'];
				$password = @$_POST['password'];
				$userid=@$_POST['update_id'];
				$ids=implode(",", $userid);
				for ($i=0; $i < $size; $i++) {
					if($userid[$i]!="new")
					{
						if($password!="")
						{
							$inserted=$wpdb->query($wpdb->prepare("UPDATE $tblname SET 
							username='".$username[$i]."',
							email='".$email[$i]."',
							password='". MD5($password[$i])."'
							WHERE subscription_id =".$subscription_id." 
							AND ID=".$userid[$i],1));
						}
						else
						{
							$inserted=$wpdb->query($wpdb->prepare("UPDATE $tblname SET 
							username='".$username[$i]."',
							email='".$email[$i]."'
							WHERE subscription_id =".$subscription_id."
							AND ID=".$userid[$i],1));
						}
					}

					elseif ($userid[$i]=="new") {
			
						$dataArr = array( 
							'subscription_id' => $subscription_id,
							'username' => $username[$i],
							'email' => $email[$i],
							'password' => MD5($password[$i]),
						);

						$inserted = $wpdb->insert($tblname, $dataArr);
						$ids = $ids.",".$wpdb->insert_id;
					}

					
				}
				if($ids!="")
				{
					
					$getnew= str_replace("new","0",$ids);
					$wpdb->query('DELETE  FROM '.$tblname.' WHERE subscription_id = "'.$subscription_id.'" AND ID NOT IN ('.$getnew.')');
					
				}
				
			}
			
			if($ispaid)
			{

				$tblname = "{$wpdb->base_prefix}CS_subs_card";
				$wpdb->get_results("SELECT * FROM ".$tblname." WHERE subscription_id = '".$subscription_id."'");
				$countpay=$wpdb->num_rows;
				$card_fname = @$_POST['card_fname'];
					$card_lname = @$_POST['card_lname'];
					$card_street = @$_POST['card_street'];
					$card_city = @$_POST['card_city'];
					$card_state = @$_POST['card_state'];
					$card_mc = @$_POST['card_mc'];
					$card_no = @$_POST['card_no'];
					$card_exm = @$_POST['card_exm'];
					$card_exy = @$_POST['card_exy'];
					$card_cvv = @$_POST['card_cvv'];
					$card_firstsubs = @$_POST['card_firstsubs'];
					$card_crntcostmonthly = @$_POST['card_crntcostmonthly'];
					$card_sts = @$_POST['card_sts'];
				if($countpay>0)
				{	
				   	
					$inserted=$wpdb->query($wpdb->prepare("UPDATE $tblname SET 
					first_name='".$card_fname."',
					last_name='".$card_lname."',
					street='".$card_street."',
					city='".$card_city."',
					state='".$card_state."',
					mc='".$card_mc."',
					cardno='".$card_no."',
					exp_m_y='".$card_exm."/".$card_exy."',
					cvc='".$card_cvv."',
					first_subscribed_date='".$card_firstsubs."',
					current_cost_monthly='".$card_crntcostmonthly."',
					active='".$card_sts."'
					WHERE subscription_id =".$subscription_id,1));	
				}
				else
				{	
				   	$dataArr = array( 
						'subscription_id' => $subscription_id,
						'first_name' => $card_fname,
						'last_name' => $card_lname,
						'street' => $card_street,
						'city' => $card_city,
						'state' => $card_state,
						'mc' => $card_mc,
						'cardno' => $card_no,
						'exp_m_y' => $card_exm."/".$card_exy,
						'cvc' => $card_cvv,
						'first_subscribed_date' => $card_firstsubs,
						'current_cost_monthly' => $card_crntcostmonthly,
						'active' => $card_sts,

					);					   
				   $inserted = $wpdb->insert($tblname, $dataArr);
				}
				 
			}
				
			
			$data['result']['status'] = $_POST['actionManual'];	
			
			echo json_encode($data);
		break;	
	}
	die;
}

//Reding count action

add_action( 'wp_ajax_reading_content_opinfact', 'readingContentOpinfact' );
add_action( 'wp_ajax_nopriv_reading_content_opinfact', 'readingContentOpinfact' );

function readingContentOpinfact() {
	global $wpdb;
	$id = $_POST['id'];
	$qry="UPDATE {$wpdb->base_prefix}posts SET 
							menu_order = menu_order + 1 
							WHERE ID=".$id;

	$inserted=$wpdb->query($wpdb->prepare($qry,1));
$autorank=1;$cat_id="";
$curr_date=current_time('Y-m-d');
	$sql = "SELECT  t1.ID,t1.key_type,SUM(t3.menu_order) as count,t2.post_id FROM {$wpdb->base_prefix}KEY_words AS t1 INNER JOIN {$wpdb->base_prefix}postmeta AS t2 ON t1.ID=t2.meta_value INNER JOIN {$wpdb->base_prefix}posts as t3 ON t2.post_id=t3.ID WHERE t2.meta_key='subject_key' AND t3.post_date LIKE '$curr_date%'  GROUP BY t1.ID ORDER BY t1.key_type ,count DESC";
	$results = $wpdb->get_results($sql);
	/*echo $wpdb->last_query;*/
	foreach ($results as $result) {
		if($result->key_type==$cat_id)
		{
			$autorank++;

		}
		else
		{
			$autorank=1;
			$cat_id=$result->key_type;
		}
		$subquery="SELECT `curr_rank`,`curr_status` FROM `{$wpdb->base_prefix}KEY_words` WHERE `ID`=".$result->ID." AND `key_type`=".$cat_id;
		$checkObj=$wpdb->get_row($subquery);
		$getcurr_rank=$checkObj->curr_rank;
		$getcurr_status=$checkObj->curr_status;
		$updateQry="";
		if($getcurr_rank>$autorank)
		{
			$updateQry="UPDATE {$wpdb->base_prefix}KEY_words SET 
							curr_rank = ".$autorank.",
							pre_rank = ".$getcurr_rank.",
							curr_status = 'up'
							WHERE ID=".$result->ID." AND key_type=".$cat_id;
		}
		elseif($getcurr_rank<$autorank)
		{

			$updateQry="UPDATE {$wpdb->base_prefix}KEY_words SET 
							curr_rank = ".$autorank.",
							pre_rank = ".$getcurr_rank.",
							curr_status = 'down'
							WHERE ID=".$result->ID." AND key_type=".$cat_id;
			
		}
		elseif($getcurr_rank==$autorank)
		{
			$updateQry="UPDATE {$wpdb->base_prefix}KEY_words SET 
							curr_rank = ".$autorank.",
							pre_rank = ".$getcurr_rank.",
							curr_status = '".$getcurr_status."'
							WHERE ID=".$result->ID." AND key_type=".$cat_id;
		}

		$updated=$wpdb->query($wpdb->prepare($updateQry,1));
		
	}

}

//Scored filter
function scoreCardFun()
{
	global $wpdb;
	$scoreHtmlreturn="";
	$termid=@($_POST['termid']);
	/*$enquescript="INNER JOIN {$wpdb->base_prefix}term_relationships on {$wpdb->base_prefix}postmeta.`post_id`={$wpdb->base_prefix}term_relationships.term_taxonomy_id INNER JOIN {$wpdb->base_prefix}term_taxonomy on {$wpdb->base_prefix}term_relationships.term_taxonomy_id={$wpdb->base_prefix}term_taxonomy.term_taxonomy_id";
	$enquewhere="{$wpdb->base_prefix}term_taxonomy.term_id='$termid'";*/
	if($termid=="all")
	{
		$scoreHtmlreturn="<table id='scoretable'><tr> 
		<th>Source</th>
    	<th><span class='rate blue active'>&nbsp;</span><span class='rate white active'>&nbsp;</span><span class='rate red active'>&nbsp;</span></th>
    	<th><span class='rate black active'>&nbsp;</span><span class='rate green active'>&nbsp;</span></th>
  		</tr>";
		$sources=$wpdb->get_results("SELECT `source` FROM {$wpdb->base_prefix}CS_subscriptions");
		foreach ($sources as $source) {
			$resource = $source->source;
			$liberal=$moderate=$conservative=$biased=$un_biased="0";
			$posts=$wpdb->get_results("SELECT {$wpdb->base_prefix}User_rating.`rate`, count(*) as count FROM `{$wpdb->base_prefix}postmeta` INNER JOIN {$wpdb->base_prefix}posts on {$wpdb->base_prefix}postmeta.`post_id`= {$wpdb->base_prefix}posts.ID INNER JOIN {$wpdb->base_prefix}User_rating ON {$wpdb->base_prefix}postmeta.`post_id`= {$wpdb->base_prefix}User_rating.`post_id` WHERE {$wpdb->base_prefix}postmeta.`meta_key`='news_source' AND {$wpdb->base_prefix}postmeta.`meta_value`='$resource' AND {$wpdb->base_prefix}posts.post_date BETWEEN NOW() - INTERVAL 30 DAY AND NOW() GROUP BY {$wpdb->base_prefix}User_rating.`rate`");
			foreach ($posts as $postId) {
				if($postId->rate=='liberal')
				{
					$liberal=$postId->count;
				}
				elseif($postId->rate=='moderate')
				{
					$moderate=$postId->count;
				}
				elseif($postId->rate=='conservative')
				{
					$conservative=$postId->count;
				}
				elseif($postId->rate=='biased')
				{
					$biased=$postId->count;
				}
				elseif($postId->rate=='un-biased')
				{
					$un_biased=$postId->count;
				}
			}
			$scoreHtmlreturn .="<tr data-liberal='$liberal' data-moderate='$moderate' data-conservative='$conservative' data-biased='$biased' data-unbiased='$un_biased'>
			<td>$resource</td>
			<td>$liberal, $moderate, $conservative</td>
			<td>$biased, $un_biased</td>
			</tr>";
		}
		$scoreHtmlreturn .="</table>";
	}
	else
	{
		$scoreHtmlreturn="<table id='scoretable'><tr> 
		<th>Source</th>
    	<th><span class='rate blue active'>&nbsp;</span><span class='rate white active'>&nbsp;</span><span class='rate red active'>&nbsp;</span></th>
    	<th><span class='rate black active'>&nbsp;</span><span class='rate green active'>&nbsp;</span></th>
  		</tr>";
		$sources=$wpdb->get_results("SELECT `source` FROM {$wpdb->base_prefix}CS_subscriptions");
		foreach ($sources as $source) {
			$resource = $source->source;
			$liberal=$moderate=$conservative=$biased=$un_biased="0";
			$posts=$wpdb->get_results("SELECT {$wpdb->base_prefix}User_rating.`rate`, count(*) as count FROM `{$wpdb->base_prefix}postmeta` INNER JOIN {$wpdb->base_prefix}posts ON {$wpdb->base_prefix}postmeta.`post_id`= {$wpdb->base_prefix}posts.ID INNER JOIN {$wpdb->base_prefix}term_relationships ON {$wpdb->base_prefix}postmeta.`post_id`={$wpdb->base_prefix}term_relationships.object_id INNER JOIN {$wpdb->base_prefix}term_taxonomy ON {$wpdb->base_prefix}term_relationships.term_taxonomy_id={$wpdb->base_prefix}term_taxonomy.term_taxonomy_id INNER JOIN {$wpdb->base_prefix}User_rating ON {$wpdb->base_prefix}postmeta.`post_id`= {$wpdb->base_prefix}User_rating.`post_id` WHERE {$wpdb->base_prefix}postmeta.`meta_key`='news_source' AND {$wpdb->base_prefix}term_taxonomy.term_id='$termid' AND {$wpdb->base_prefix}postmeta.`meta_value`='$resource' AND {$wpdb->base_prefix}posts.post_date BETWEEN NOW() - INTERVAL 30 DAY AND NOW() GROUP BY {$wpdb->base_prefix}User_rating.`rate`");
			foreach ($posts as $postId) {
				if($postId->rate=='liberal')
				{
					$liberal=$postId->count;
				}
				elseif($postId->rate=='moderate')
				{
					$moderate=$postId->count;
				}
				elseif($postId->rate=='conservative')
				{
					$conservative=$postId->count;
				}
				elseif($postId->rate=='biased')
				{
					$biased=$postId->count;
				}
				elseif($postId->rate=='un-biased')
				{
					$un_biased=$postId->count;
				}
			}
			$scoreHtmlreturn .="<tr data-liberal='$liberal' data-moderate='$moderate' data-conservative='$conservative' data-biased='$biased' data-unbiased='$un_biased'>
			<td>$resource</td>
			<td>$liberal, $moderate, $conservative</td>
			<td>$biased, $un_biased</td>
			</tr>";
		}
		$scoreHtmlreturn .="</table>";
	}
	
	echo $scoreHtmlreturn;die;

}

add_action( 'wp_ajax_scored_filterd', 'scoreCardFun' );
add_action( 'wp_ajax_nopriv_scored_filterd', 'scoreCardFun' );

//Filtred reating
function ratingFilterFunction()
{
	$data=array();
	$rating_type=@$_POST['rating_type'];
	$dateType=@$_POST['dateType'];
	global $wpdb;
	$currentDate = current_time('Y-m-d');
	if($dateType==2)
	$currentDate = date('Y-m-d', strtotime('-1 day', strtotime(current_time('Y-m-d'))));
	$table1="{$wpdb->base_prefix}posts";
	$table2="{$wpdb->base_prefix}postmeta";
	if($rating_type!="")
	$getresult = $wpdb->get_results("SELECT * FROM $table1 
	INNER JOIN $table2 ON $table1.ID = $table2.post_id
	 WHERE $table1.post_date like '$currentDate%' AND $table1.post_type='news' AND $table1.post_status='publish' AND $table2.`meta_key`='rating_type' AND $table2.`meta_value`='$rating_type'");
	else
		$getresult = $wpdb->get_results("SELECT * FROM $table1 WHERE $table1.post_date like '$currentDate%' AND $table1.post_type='news' AND $table1.post_status='publish'");

	$html="<ul>";
	foreach ($getresult as $row) {
		$getrating = $wpdb->get_row("SELECT `meta_value` FROM {$wpdb->base_prefix}postmeta WHERE `post_id`='".$row->ID."' AND `meta_key`='rating_type'");
		$showrating=$getrating->meta_value;
		$spanvalue="<span class='rate black'>&nbsp;</span><span class='rate green'>&nbsp;</span>";
		if($showrating=='un-biased')
			$spanvalue="<span class='rate black'>&nbsp;</span><span class='rate green active'>&nbsp;</span>";
		elseif($showrating=='biased')
			$spanvalue="<span class='rate black active'>&nbsp;</span><span class='rate green'>&nbsp;</span>";
		$term_obj_list = get_the_terms( $row->ID, 'newscategories' );
		foreach ($term_obj_list as $loop) {
			if($loop->name=="Politics")
			{
				$spanvalue="<span class='rate blue'>&nbsp;</span><span class='rate white'>&nbsp;</span><span class='rate red'>&nbsp;</span>";
				if($showrating=='liberal')
					$spanvalue="<span class='rate blue active'>&nbsp;</span><span class='rate white'>&nbsp;</span><span class='rate red'>&nbsp;</span>";
				elseif ($showrating=='moderate')
					$spanvalue="<span class='rate blue'>&nbsp;</span><span class='rate white active'>&nbsp;</span><span class='rate red'>&nbsp;</span>";
				elseif ($showrating=='conservative')
					$spanvalue="<span class='rate blue'>&nbsp;</span><span class='rate white'>&nbsp;</span><span class='rate red active'>&nbsp;</span>";
			}
		}
		$jdata=json_decode($row->post_excerpt);
		/*echo "<pre>";print_r($jdata);echo "</pre>";*/
		$html .="<li id='".$row->ID."' class='readContent'><span class='rate-group'>".$spanvalue."</span><a href='".get_permalink( $row->ID )."' target='_blank'>".@$row->post_title."</a><span class='auth-name ".$showrating."-text'>".@$jdata->author."</span></li>";
	}
	$html .="</ul>";
	$data['today']=$html;

	$pDate=date('Y-m-d', strtotime('-1 day', strtotime(current_time('Y-m-d'))));
	if($dateType==2)
	$pDate = date('Y-m-d', strtotime('-2 day', strtotime(current_time('Y-m-d'))));
	if($rating_type!="")
		$getresult = $wpdb->get_results("SELECT * FROM $table1 
	INNER JOIN $table2 ON $table1.ID = $table2.post_id
	 WHERE $table1.post_date like '$pDate%' AND $table1.post_type='news' AND $table1.post_status='publish' AND $table2.`meta_key`='rating_type' AND $table2.`meta_value`='$rating_type'");
	else
		$getresult = $wpdb->get_results("SELECT * FROM $table1 WHERE $table1.post_date like '$pDate%' AND $table1.post_type='news' AND $table1.post_status='publish'");
	$html ="<ul>";
	foreach ($getresult as $row) {
		$getrating = $wpdb->get_row("SELECT `meta_value` FROM {$wpdb->base_prefix}postmeta WHERE `post_id`='".$row->ID."' AND `meta_key`='rating_type'");
		$showrating=$getrating->meta_value;
		$spanvalue="<span class='rate black'>&nbsp;</span><span class='rate green'>&nbsp;</span>";
		if($showrating=='un-biased')
			$spanvalue="<span class='rate black'>&nbsp;</span><span class='rate green active'>&nbsp;</span>";
		elseif($showrating=='biased')
			$spanvalue="<span class='rate black active'>&nbsp;</span><span class='rate green'>&nbsp;</span>";
		$term_obj_list = get_the_terms( $row->ID, 'newscategories' );
		foreach ($term_obj_list as $loop) {
			if($loop->name=="Politics")
			{
				$spanvalue="<span class='rate blue'>&nbsp;</span><span class='rate white'>&nbsp;</span><span class='rate red'>&nbsp;</span>";
				if($showrating=='liberal')
					$spanvalue="<span class='rate blue active'>&nbsp;</span><span class='rate white'>&nbsp;</span><span class='rate red'>&nbsp;</span>";
				elseif ($showrating=='moderate')
					$spanvalue="<span class='rate blue'>&nbsp;</span><span class='rate white active'>&nbsp;</span><span class='rate red'>&nbsp;</span>";
				elseif ($showrating=='conservative')
					$spanvalue="<span class='rate blue'>&nbsp;</span><span class='rate white'>&nbsp;</span><span class='rate red active'>&nbsp;</span>";
			}
		}
		$jdata=json_decode($row->post_excerpt);
		/*echo "<pre>";print_r($jdata);echo "</pre>";*/
		$html .="<li id='".$row->ID."' class='readContent'><span class='rate-group'>".$spanvalue."</span><a href='".get_permalink( $row->ID )."' target='_blank'>".@$row->post_title."</a><span class='auth-name ".$showrating."-text'>".@$jdata->author."</span></li>";
	}
	$html .="</ul>";
	$data['yesterday']=$html;
	
	echo json_encode($data);die;

}

add_action( 'wp_ajax_today_rating_filter', 'ratingFilterFunction' );
add_action( 'wp_ajax_nopriv_today_rating_filter', 'ratingFilterFunction' );


function admin_rating_counter_function_ioptn() {
	global $wpdb;
	$getID=get_the_ID();
	$user = wp_get_current_user();
	$getuserId= $user->ID;

	$getrating = $wpdb->get_row("SELECT `meta_value` FROM {$wpdb->base_prefix}postmeta WHERE `post_id`='".$getID."' AND `meta_key`='rating_type'");
		$showrating=$getrating->meta_value;
	?><div class="opin-rating">
					<h3 class="rating-title">OpinFacts Rating</h3>
					<div class="rating-box">
						<div class="pol-box">
							<h4>Political</h4>
							<span class="rate-group">
								<span class="rate blue <?php if($showrating=='liberal') echo 'active'; ?> ">&nbsp;</span>
								<span class="rate white <?php if($showrating=='moderate') echo 'active'; ?>">&nbsp;</span>
								<span class="rate red <?php if($showrating=='conservative') echo 'active'; ?>">&nbsp;</span>
							</span>
						</div>
						<div class="non-pol-box">
							<h4>Non-Political</h4>
							<span class="rate-group">
								<span class="rate black <?php if($showrating=='biased') echo 'active'; ?>">&nbsp;</span>
								<span class="rate green <?php if($showrating=='un-biased') echo 'active'; ?>">&nbsp;</span>
							</span>
						</div>
					</div>
				</div><?php
	
}

add_shortcode('admin_rating_counter_ioptn', 'admin_rating_counter_function_ioptn');

