<?php
/**
 * Adds News_Widget widget.
 */
class News_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'news_widget', // Base ID
			esc_html__( 'News', 'text_domain' ), // Name
			array( 'description' => esc_html__( 'News Widget', 'text_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of left side widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		global $wpdb;
		$returnvar="";
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			if($instance['title']=="Current News")
			{
				echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
				$currentDate = current_time('Y-m-d');
				$getresult = $wpdb->get_results("SELECT * FROM {$wpdb->base_prefix}posts WHERE `post_date` LIKE '".$currentDate."%' AND post_type='news' AND post_status='publish' ORDER BY `menu_order` DESC LIMIT 5 ");
				$countPost=$wpdb->num_rows;
	if($countPost<1)
	{
		$currentDate=date('Y-m-d', strtotime("-1 day", strtotime(current_time('Y-m-d'))));
		$getresult = $wpdb->get_results("SELECT * FROM {$wpdb->base_prefix}posts WHERE `post_date` LIKE '".$currentDate."%' AND post_type='news' AND post_status='publish' ORDER BY `menu_order` DESC LIMIT 5 ");
	}

					$returnvar=  "<div class='textwidget news-text'><ul>";
					foreach ($getresult as $row) {
						$jdata=json_decode($row->post_excerpt);
						$returnvar .= "<li id='".$row->ID."' class='readContent'><a href='".get_permalink( $row->ID )."' target='_blank'>".$row->post_title."</a></li>";
					}
					 	
					$returnvar .= "</ul></div>";
			}
			elseif($instance['title']=="Most Read – Last 24 Hours") {

				echo "<h2 class='widgettitle'>Most Read <span class='last-time'>Last 24 Hours</span></h2>";
				$currentDate = date('Y-m-d', strtotime('-1 day', strtotime(current_time('Y-m-d'))));
				$getresult = $wpdb->get_results("SELECT * FROM {$wpdb->base_prefix}posts WHERE `post_date` LIKE '".$currentDate."%' AND post_type='news' AND post_status='publish' ORDER BY `menu_order` DESC LIMIT 5 ");

					$returnvar=  "<div class='textwidget news-text'><ul>";
					foreach ($getresult as $row) {
						$jdata=json_decode($row->post_excerpt);
						$returnvar .= "<li id='".$row->ID."' class='readContent'><a href='".get_permalink( $row->ID )."' target='_blank'>".$row->post_title."</a></li>";
					}
					 	
					$returnvar .= "</ul></div>";
			}
			elseif($instance['title']=="Most Read – Last 7 Days")
			{
				echo "<h2 class='widgettitle'>Most Read <span class='last-time'>Last 7 Days</span></h2>";
				$currentDate = current_time('Y-m-d');
				$LSdate = date('Y-m-d', strtotime('-7 day', strtotime(current_time('Y-m-d'))));
				$LSdate=$LSdate." 00:00:00"; $currentDate=$currentDate." 00:00:00";
				$getresult = $wpdb->get_results("SELECT * FROM {$wpdb->base_prefix}posts WHERE post_type='news' AND post_status='publish' AND `post_date` BETWEEN '".$LSdate."' AND '".$currentDate."' ORDER BY `menu_order` DESC LIMIT 5");

					$returnvar=  "<div class='textwidget news-text'><ul>";
					foreach ($getresult as $row) {
						$jdata=json_decode($row->post_excerpt);
						$returnvar .= "<li id='".$row->ID."' class='readContent'><a href='".get_permalink( $row->ID )."' target='_blank'>".$row->post_title."</a></li>";
					}
					 	
					$returnvar .= "</ul></div>";
			}
		}
		
		echo $returnvar;
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'New title', 'text_domain' );
		?>
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'text_domain' ); ?></label> 
		<?php /* <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>"> */ ?>
		<select id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" class="widefat">
           
        <option <?php selected( $instance['title'], "Current News" ); ?> value="Current News">Current News</option>
        <option <?php selected( $instance['title'], "Most Read – Last 24 Hours" ); ?> value="Most Read – Last 24 Hours">Most Read – Last 24 Hours</option>
        <option <?php selected( $instance['title'], "Most Read – Last 7 Days" ); ?> value="Most Read – Last 7 Days">Most Read – Last 7 Days</option>

              
        </select>
		</p>

		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? sanitize_text_field( $new_instance['title'] ) : '';

		return $instance;
	}

} // class News_Widget

function register_news_widget() {
    register_widget( 'News_Widget' );
}
add_action( 'widgets_init', 'register_news_widget' );





/**
 * Adds Trending_Widget widget.
 */
class Trending_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'trending_widget', // Base ID
			esc_html__( 'Trending', 'text_domain' ), // Name
			array( 'description' => esc_html__( 'Trending Widget', 'text_domain' ), ) // Args
		);
	}


	/**
	 * Front-end display of right side widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		global $wpdb;
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			
			echo "<h2 class='widgettitle'>Trending Subjects <span class='last-time'>Last 24 Hours</span></h2>";
			
		}
		
	    $currentDate = current_time('Y-m-d');
	    $getresulTest = $wpdb->get_results("SELECT * FROM {$wpdb->base_prefix}posts WHERE `post_date` LIKE '".$currentDate."%' AND post_type='news' AND post_status='publish' ORDER BY `menu_order` DESC LIMIT 5 ");
	    $countPost=$wpdb->num_rows;
	if($countPost<1)
		$currentDate=date('Y-m-d', strtotime("-1 day", strtotime(current_time('Y-m-d'))));
	 $termscat=$wpdb->get_results("SELECT sum(post.`menu_order`) as count,rel.`term_taxonomy_id`,terms.name FROM `{$wpdb->base_prefix}posts` as post INNER JOIN {$wpdb->base_prefix}term_relationships as rel ON post.ID=rel.`object_id` INNER JOIN {$wpdb->base_prefix}terms as terms ON rel.`term_taxonomy_id`= terms.term_id WHERE `post_date` LIKE '$currentDate%' AND post.post_type='news' GROUP BY rel.`term_taxonomy_id` ORDER BY count DESC limit 4");

	    $rightWhtml="<div class='textwidget trending-text'>";
	   
	    foreach($termscat as $customterm)
		{

			$subrightWhtml="";
			$countPost=0;
			$getkey=$wpdb->get_results("SELECT * FROM {$wpdb->base_prefix}KEY_words WHERE `key_type`=".$customterm->term_taxonomy_id." ORDER BY curr_rank");
			$countKey=$wpdb->num_rows;
			$limitcount=1;
			if($countKey>0)
			{
				$subrightWhtml .="<h3 class='".$customterm->count."'>".$customterm->name."</h3><ul>";
				foreach ($getkey as $value) {
					$getcountpost=$wpdb->get_results("SELECT  {$wpdb->base_prefix}postmeta.`post_id` FROM {$wpdb->base_prefix}postmeta INNER JOIN {$wpdb->base_prefix}posts ON {$wpdb->base_prefix}postmeta.`post_id`= {$wpdb->base_prefix}posts.`ID` WHERE {$wpdb->base_prefix}postmeta.`meta_key`='subject_key' AND {$wpdb->base_prefix}postmeta.`meta_value`='".$value->ID."' AND {$wpdb->base_prefix}posts.`post_status`='publish' AND {$wpdb->base_prefix}posts.`post_date` like '$currentDate%'");
					$countPostvar=$wpdb->num_rows;
					if($countPostvar>0 && $limitcount<7)
					{
						$subrightWhtml .="<li class='".$value->curr_status."'><a href='".site_url()."/trending-key?getid=".$value->ID."'>".$value->keyword."</a></li>";
						$countPost=1;
						$limitcount++;
					}

					
				}
				$subrightWhtml .="</ul>";
				if($countPost==1)
				{
					$rightWhtml .=$subrightWhtml;
				}

			}
			
		}
		 $rightWhtml .="</div>";
		 echo $rightWhtml;
		
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : "Trending Subjects – Last 24 hours";
		?>
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'text_domain' ); ?></label> 
		 <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>"> 
		
		</p>

		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? sanitize_text_field( $new_instance['title'] ) : '';

		return $instance;
	}

} // class Trending_Widget

function register_trending_widget() {
    register_widget( 'Trending_Widget' );
}
add_action( 'widgets_init', 'register_trending_widget' );





/**
 * Adds Scorecard_Widget widget.
 */
class Scorecard_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'score_widget', // Base ID
			esc_html__( 'Scorecard', 'text_domain' ), // Name
			array( 'description' => esc_html__( 'Scorecard Widget', 'text_domain' ), ) // Args
		);
	}


	/**
	 * Front-end display of right side widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		global $wpdb;
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			
			echo "<h2 class='widgettitle'>Scorecard <span class='last-time'>Last 30 Days</span></h2>";
			
		}
		$termscat = get_terms([
		    'taxonomy' => 'newscategories',
		    'hide_empty' => false,
	    ]);
	   //print_r($termscat);
		?><div class="widget-filter">
			<select id="filterd" name="filterd" autocomplete="off">
			   <option value="" disabled selected>Filtered By</option>
			   <option value="all">All</option>
			   <?php foreach ($termscat as $value) { 
			   	echo "<option value='".$value->term_id."'>$value->name</option>";
			    } ?>
			</select>
			<div class="sort-wrap">
			<select id="sorted" name="sorted" autocomplete="off">
			   <option value="" disabled selected>Sort By</option>
			   <option value="liberal">Blue</option>
			   <option value="moderate">White</option>
			   <option value="conservative">Red</option>
			   <option value="biased">Black</option>
			   <option value="unbiased">Green</option>
			</select>
			<a class="sort-heigh-low hidden" id="sortDnA" data-value='desc' href="javascript:;"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a>
			</div>
		</div>
		<script>
		jQuery(document).ready(function(){
			jQuery("#sorted").val('');
			jQuery("#filterd").val('');
			function sortingscript()
			{
				var shortval=jQuery("#sorted").val();
				if(shortval!="")
				{
					
					var orderby=jQuery("#sortDnA").attr("data-value");
					/*sorting script*/

					var table, rows, switching, i, x, y, shouldSwitch;
					table = document.getElementById("scoretable");
					switching = true;
					while (switching) {
						switching = false;
						rows = table.getElementsByTagName("TR");
						for (i = 1; i < (rows.length - 1); i++) {
							shouldSwitch = false;
							x = rows[i].getAttribute("data-"+shortval);
							y = rows[i + 1].getAttribute("data-"+shortval);
							if(orderby=='desc')
							{
								if (parseInt(x) < parseInt(y)) {
									shouldSwitch = true;
									break;
								}
							}
							else
							{
								if (parseInt(x) > parseInt(y)) {
									shouldSwitch = true;
									break;
								}
							}
							
						}
						if (shouldSwitch) {

							rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
							switching = true;
						}
					}
					/*End sorting script*/

				}
			}
			jQuery(document).on('change','#filterd',function(){
				var termid=jQuery(this).val();
				
				if(termid)
				{
					jQuery.ajax({
		                type : "POST",
		                url : "<?php echo admin_url('admin-ajax.php'); ?>/",
		                data : {
		                	action: "scored_filterd",
		                	termid: termid
		                },
		                async: false,
		                success: function(response) {
		                	jQuery("#appendscored").html(response);
	                    }
		            });
				}
				sortingscript();
			});
			
			jQuery(document).on('change','#sorted',function(){
				jQuery("#sortDnA").removeClass('hidden');
				sortingscript();
			});
			jQuery(document).on('click','#sortDnA',function(){
				var dnavalue=jQuery(this).attr("data-value");
				if(dnavalue=='desc')
				{
					jQuery(this).attr('data-value' , 'asc');
					jQuery(this).find('span').removeClass('glyphicon-arrow-up');
					jQuery(this).find('span').addClass('glyphicon-arrow-down');
				}
				else
				{
					jQuery(this).attr('data-value' , 'desc');
					jQuery(this).find('span').removeClass('glyphicon-arrow-down');
					jQuery(this).find('span').addClass('glyphicon-arrow-up');
				}
				sortingscript();
			});
		});
		</script>
		<?php 
		$scoreHtml="<div id='appendscored'><table id='scoretable'><tr> 
		<th>Source</th>
    	<th><span class='rate blue active'>&nbsp;</span><span class='rate white active'>&nbsp;</span><span class='rate red active'>&nbsp;</span></th>
    	<th><span class='rate black active'>&nbsp;</span><span class='rate green active'>&nbsp;</span></th>
  		</tr>";
		$sources=$wpdb->get_results("SELECT `source` FROM {$wpdb->base_prefix}CS_subscriptions");
		foreach ($sources as $source) {
			$resource = $source->source;
			$liberal=$moderate=$conservative=$biased=$un_biased="0";
			$posts=$wpdb->get_results("SELECT {$wpdb->base_prefix}User_rating.`rate`, count(*) as count FROM `{$wpdb->base_prefix}postmeta` INNER JOIN {$wpdb->base_prefix}posts on {$wpdb->base_prefix}postmeta.`post_id`= {$wpdb->base_prefix}posts.ID INNER JOIN {$wpdb->base_prefix}User_rating ON {$wpdb->base_prefix}postmeta.`post_id`= {$wpdb->base_prefix}User_rating.`post_id` WHERE {$wpdb->base_prefix}postmeta.`meta_key`='news_source' AND {$wpdb->base_prefix}postmeta.`meta_value`='$resource' AND {$wpdb->base_prefix}posts.post_date BETWEEN NOW() - INTERVAL 31 DAY AND NOW() - INTERVAL 1 DAY GROUP BY {$wpdb->base_prefix}User_rating.`rate`");
			/*echo $wpdb->last_query;*/
			foreach ($posts as $postId) {
				if($postId->rate=='liberal')
				{
					$liberal=$postId->count;
				}
				elseif($postId->rate=='moderate')
				{
					$moderate=$postId->count;
				}
				elseif($postId->rate=='conservative')
				{
					$conservative=$postId->count;
				}
				elseif($postId->rate=='biased')
				{
					$biased=$postId->count;
				}
				elseif($postId->rate=='un-biased')
				{
					$un_biased=$postId->count;
				}
			}
			$scoreHtml .="<tr data-liberal='$liberal' data-moderate='$moderate' data-conservative='$conservative' data-biased='$biased' data-unbiased='$un_biased'>
			<td>$resource</td>
			<td>$liberal, $moderate, $conservative</td>
			<td>$biased, $un_biased</td>
			</tr>";
		}
		$scoreHtml .="</table></div>";
		echo $scoreHtml;
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : "Scorecard – Last 30 Days";
		?>
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'text_domain' ); ?></label> 
		 <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>"> 
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? sanitize_text_field( $new_instance['title'] ) : '';

		return $instance;
	}

} // class Scorecard_Widget

function register_scorecard_widget() {
    register_widget( 'Scorecard_Widget' );
}
add_action( 'widgets_init', 'register_scorecard_widget' );
